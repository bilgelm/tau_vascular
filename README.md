# tau_vascular

Code for running the statistical analyses and generating plots presented in the paper "Vascular risk is not associated with PET measures of Alzheimer’s disease neuropathology among cognitively normal older adults", [medRxiv preprint](https://www.medrxiv.org/content/10.1101/2021.09.01.21256275v1), [deidentified dataset](https://doi.org/10.7910/DVN/JOMOEN)
